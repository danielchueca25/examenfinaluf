package com.daniel.finaluf1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class DetailsFragment extends Fragment {

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //make List,Grid,RecyclerView
    }

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final int ARG_PARAM5 = 1;
    private static final ArrayList<Integer> ARG_PARAM6 = new ArrayList<>();

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String mParam3;
    private String mParam4;
    private int mParam5;
    private ArrayList<Integer> mParam6;


    public DetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2,String param3, String param4, int param5, ArrayList<Integer> param6) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        args.putInt(String.valueOf(ARG_PARAM5), param5);
        args.putIntegerArrayList(String.valueOf(ARG_PARAM6), param6);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        TextView guide = view.findViewById(R.id.guideDetail);
        TextView city = view.findViewById(R.id.cityDetails);
        ImageView imageView = view.findViewById(R.id.imageCircle);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerImages);
        TextView price = view.findViewById(R.id.priceDetails);
        TextView desc = view.findViewById(R.id.descDetails);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            mParam3 = getArguments().getString(ARG_PARAM3);
            mParam4 = getArguments().getString(ARG_PARAM4);
            mParam5 = getArguments().getInt(String.valueOf(ARG_PARAM5));
            mParam6 = getArguments().getIntegerArrayList(String.valueOf(ARG_PARAM6));
            guide.setText(mParam1);
            city.setText(mParam2);
            price.setText(mParam3);
            desc.setText(mParam4);
            Picasso.get().load(mParam5)
                    .fit()
                    .centerCrop()
                    .into(imageView);
            MyAdapter2 myAdapter2 = new MyAdapter2(mParam6,DetailsFragment.this);
            recyclerView.setAdapter(myAdapter2);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        }



        return view;

    }
}