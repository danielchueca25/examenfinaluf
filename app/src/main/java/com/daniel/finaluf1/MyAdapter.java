package com.daniel.finaluf1;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter <MyAdapter.MyViewHolder> {
    private Fragment context;
    private ArrayList<Guide> guides;
    private DetailsFragment detailsFragment;
    public MyAdapter(ArrayList<Guide> guides, Fragment context) {
        this.context = context;
        this.guides = guides;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        private ImageView imageView;
        private TextView guide;
        private TextView city;
        private TextView price;

        RelativeLayout rowData;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            guide= itemView.findViewById(R.id.guide);
            city= itemView.findViewById(R.id.city);
            price= itemView.findViewById(R.id.price);
            imageView= itemView.findViewById(R.id.imageParc);
            rowData = itemView.findViewById(R.id.rowData);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context.getContext());
        View view = inflater.inflate(R.layout.grid_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(guides.get(position).getGuideImage())
                .fit()
                .centerCrop()
                .into(holder.imageView);

        holder.guide.setText(guides.get(position).getGuideName());
        holder.city.setText(guides.get(position).getGuideCity());
        holder.price.setText(guides.get(position).getGuidePrice());
        holder.rowData.setOnClickListener(v -> {
             detailsFragment= DetailsFragment.newInstance(guides.get(holder.getAdapterPosition()).getGuideName(),
                    guides.get(holder.getAdapterPosition()).getGuideCity(), guides.get(holder.getAdapterPosition()).getGuidePrice(),
                     guides.get(holder.getAdapterPosition()).getGuideDesc(),guides.get(holder.getAdapterPosition()).getGuideImage(),
                     guides.get(holder.getAdapterPosition()).getGuideImages()
             );
            FragmentManager fragmentManager= ((FragmentActivity)context.getContext()).getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.main_frame,detailsFragment);
            fragmentTransaction.commit();
        });

    }

    @Override
    public int getItemCount() {
        return guides.size();
    }


    }

